#!/bin/bash

# Generate log file
LOG_FILE="logs/$( date '+%F-%T' )_$( hostname )_kiln_log.txt";
# initiate kiln daemon
./run_logic.py > LOG_FILE;

