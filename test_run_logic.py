#!/usr/bin/python3

from run_logic import *

with open('test_env_values.json') as jfile:
        test_params = json.loads(jfile.read())
    kiln = KilnOperationServer(name="TEST_KILN")
    with open("test_stages.json") as stage_file:
    	stages = json.loads(stage_file.read())
    plan = FiringPlan(stages, "C", kiln, program_name="TEST_PROGRAM")